package paquete;

import java.util.HashMap;
import java.util.List;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Session;
import spark.template.velocity.VelocityTemplateEngine;

public class ControladorAdmin {

    // Corroborar que se inicio sesion, sino salir al index principal
    public static Route getIndex = (Request req, Response res) -> {
        boolean admin = false;
        try {
            admin = req.session().attribute("admin");
        } catch (Exception e) {
            res.redirect("/api/index");
        }
        if (admin) {
            ItemDao iDao = new ItemDao();
            List<Item> items = iDao.getAllItems();
            HashMap<String, Object> model = new HashMap<>();
            model.put("items", items);
            model.put("template", "./resources/templates/admin/index.vtl");
            return new VelocityTemplateEngine()
                    .render(new ModelAndView(model, "/resources/templates/admin/layout.vtl"));
        } else {
            res.redirect("/api/index");
            return null;
        }
    };
    // CHEKEAR SI ES ADMINISTRADOR
    public static Route addItem = (Request req, Response res) -> {
        HashMap<String, Object> model = new HashMap<>();

        if (req.queryParams("enviar") != null) {
            Item item = new Item();
            boolean error = false;
            boolean verdad = false;
            try {
                item.setDescripcion(req.queryParams("descripcion"));
                item.setCobertura(Float.parseFloat(req.queryParams("cobertura")));
                item.setTipo(req.queryParams("tipo"));
                item.setValor_mensual(Float.parseFloat(req.queryParams("mensualidad")));
                ItemDao iDao = new ItemDao();
                verdad = iDao.addItem(item);
            } catch (Exception e) {
                System.out.println(e);
                error = true;
                res.redirect("index");
            }
            if (verdad) {
                res.redirect("/api/admin/index");
            } else {
                // ACA TENGO QUE PONER ALGO PARA DESPUES MOSTRAR EN EL VTL.
            }
        } else {
            ItemDao aux = new ItemDao();
            model.put("categorias", aux.getCategorias());
            model.put("template", "./resources/templates/admin/addItem.vtl");
        }
        return new VelocityTemplateEngine().render(new ModelAndView(model, "/resources/templates/admin/layout.vtl"));
    };

    public static Route getItemsHogar = (Request req, Response res) -> {
        HashMap<String, Object> model = new HashMap<>();
        boolean admin = false;
        try {
            admin = req.session().attribute("admin");
        } catch (Exception e) {
            res.redirect("/api/index");
        }
        if (admin) {
            ItemDao iDao = new ItemDao();
            List<Item> items = iDao.getItems("Hogar");
            model.put("items", items);
            model.put("template", "./resources/templates/admin/verCategoria.vtl");
            return new VelocityTemplateEngine()
                    .render(new ModelAndView(model, "/resources/templates/admin/layout.vtl"));
        } else {
            res.redirect("/api/index");
        }
        return null;
    };

    public static Route getItemsPersona = (Request req, Response res) -> {
        HashMap<String, Object> model = new HashMap<>();
        boolean admin = false;
        try {
            admin = req.session().attribute("admin");
        } catch (Exception e) {
            res.redirect("/api/index");
        }
        if (admin) {
            ItemDao iDao = new ItemDao();
            List<Item> items = iDao.getItems("Persona");
            model.put("items", items);
            model.put("template", "./resources/templates/admin/verCategoria.vtl");
            return new VelocityTemplateEngine()
                    .render(new ModelAndView(model, "/resources/templates/admin/layout.vtl"));
        } else {
            res.redirect("/api/index");
        }
        return null;
    };

    public static Route getItemsAutomovil = (Request req, Response res) -> {
        HashMap<String, Object> model = new HashMap<>();
        boolean admin = false;
        try {
            admin = req.session().attribute("admin");
        } catch (Exception e) {
            res.redirect("/api/index");
        }
        if (admin) {
            ItemDao iDao = new ItemDao();
            List<Item> items = iDao.getItems("Automovil");
            model.put("items", items);
            model.put("template", "./resources/templates/admin/verCategoria.vtl");
            return new VelocityTemplateEngine()
                    .render(new ModelAndView(model, "/resources/templates/admin/layout.vtl"));
        } else {
            res.redirect("/api/index");
        }
        return null;
    };

    public static Route eliminarItem = (Request req, Response res) -> {
        int idItem = -1;
        HashMap<String, Object> model = new HashMap<>();
        boolean admin = false;
        try {
            admin = req.session().attribute("admin");
        } catch (Exception e) {
            res.redirect("/api/index");
        }
        if (admin) {
            ItemDao iDao = new ItemDao();
            try {
                idItem = Integer.parseInt(req.queryParams("codigo"));
            } catch (Exception e) {
                System.out.println(e);
                res.redirect("/api/admin/index");
                return null;
            }
            iDao.eliminarItem(idItem);
            res.redirect("/api/admin/index");
        }
        return null;
    };

    public static Route modificarItem = (Request req, Response res) -> {
        int idItem = -1;
        HashMap<String, Object> model = new HashMap<>();
        boolean admin = false;
        try {
            admin = req.session().attribute("admin");
        } catch (Exception e) {
            res.redirect("/api/index");
        }
        if (admin) {
            if (req.queryParams("enviar") != null) {
                Item item = new Item();
                boolean error = false;
                boolean verdad = false;
                try {
                    item.setDescripcion(req.queryParams("descripcion"));
                    item.setCobertura(Float.parseFloat(req.queryParams("cobertura")));
                    item.setTipo(req.queryParams("tipo"));
                    item.setValor_mensual(Float.parseFloat(req.queryParams("mensualidad")));
                    item.setCodigo(Integer.parseInt(req.queryParams("codigo")));
                    ItemDao iDao = new ItemDao();
                    verdad = iDao.actualizarItem(item);
                } catch (Exception e) {
                    System.out.println(e);
                    error = true;
                    res.redirect("/api/admin/index");
                }
                res.redirect("/api/admin/index");
            } else {
                ItemDao aux = new ItemDao();
                Item item=new Item();
                try {
                    int id=Integer.parseInt(req.queryParams("codigo"));
                    item=aux.getItem(id);
                    model.put("item", item);
                } catch (Exception e) {
                    System.out.println(e);
                }
                model.put("categorias", aux.getCategorias());
                model.put("template", "./resources/templates/admin/modificarItem.vtl");
                return new VelocityTemplateEngine()
                .render(new ModelAndView(model, "/resources/templates/admin/layout.vtl"));
            }
        }else{
            res.redirect("api/admin/index");
        }
        return null;
    };
}