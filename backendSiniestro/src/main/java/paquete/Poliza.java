package paquete;
import lombok.Data;

@Data
public class Poliza{
    private int nroPoliza;
    private String fecInicio;
    private String fecVencimiento;
    private String fecPago;
    private double precio;
    private int seguro_idSeguro;
    private String seguro_dni_cliente;

    public Poliza(){
        this.nroPoliza=0;
        this.fecInicio="";
        this.fecVencimiento="";
        this.fecPago="";
        this.precio=0;
        this.seguro_idSeguro=0;
        this.seguro_dni_cliente="";
    }
}