package paquete;

import org.sql2o.Connection;

import util.Sql2oDAO;

import java.io.FileOutputStream;

import com.itextpdf.text.BaseColor;
//librerias necesarias para crear el pdf
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PolizaDao{
    private static Poliza poliza=new Poliza();
    private static String nombreCliente="";

    private static Poliza getDatos(int idSeguro){
        Poliza aux=new Poliza();

        try (Connection con =Sql2oDAO.getSql2o().open()){
            String sql="SELECT * FROM POLIZA WHERE SEGURO_IDSEGURO="+idSeguro;
            aux=con.createQuery(sql).executeAndFetchFirst(Poliza.class);
            int nroPoliza=aux.getNroPoliza();
            String getNombre="SELECT CLIENTE.NOMBRE FROM CLIENTE,POLIZA WHERE POLIZA.NROPOLIZA="+nroPoliza+" AND CLIENTE.DNI=POLIZA.SEGURO_DNI_CLIENTE";
            System.out.println(getNombre);
            nombreCliente=con.createQuery(getNombre).executeAndFetchFirst(String.class);
            System.out.println(nombreCliente);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        
        return aux;
    }

    public static Poliza getPoliza(){
        return poliza;
    }
    public static String getNombreCliente(){
        return nombreCliente;
    }


    public static String generarFactura(int id,String tipo){

        poliza=getDatos(id);
        Document documento=new Document();
        String rutaResultado="";
        try {
            String rutaDestino="src/main/java/resources/public/facturas";
            String rutaLogo="src/main/java/resources/public/image/logoRicardo.png";
            long tiempo=System.currentTimeMillis();
            PdfWriter.getInstance(documento, new FileOutputStream(rutaDestino+"/Reporte"+tiempo+".pdf"));
            documento.open();
            Image imagen=Image.getInstance(rutaLogo);
            //imagen.setAbsolutePosition(10, 5);
            
            PdfPTable tabla=new PdfPTable(2);   //cantidad de columnas
            PdfPCell celda=new PdfPCell();
            Paragraph titulo=new Paragraph("Ricardo Ruibal");
            Font fuente=new Font(FontFamily.TIMES_ROMAN);
            fuente.setColor(new BaseColor(255,255,255));
            titulo.setFont(fuente);            
            
            titulo.setAlignment(Element.ALIGN_CENTER);
            celda.addElement(titulo);
            celda.setBackgroundColor(new BaseColor(81, 127, 158));
            //celda.setColspan(2);
            //celda.setRowspan(2);
            tabla.addCell(celda);
            
            imagen.setWidthPercentage(2);
            tabla.addCell(imagen);

            tabla.addCell("Inicio cobertura: "+poliza.getFecInicio());
            tabla.addCell("Fecha actual: "+java.time.LocalDate.now());      
            tabla.addCell("Fin cobertura: "+poliza.getFecVencimiento());
            tabla.addCell("Cliente: "+nombreCliente);
            tabla.addCell("Tipo seguro: "+tipo);
            tabla.addCell("Dni: "+poliza.getSeguro_dni_cliente());
            //celda.setBackgroundColor(new BaseColor(255,255,255));
            //celda.setPhrase(new Phrase("Id seguro"));
            //tabla.addCell(celda);
            tabla.addCell("Id seguro: "+poliza.getSeguro_idSeguro());
            tabla.addCell("");
            tabla.addCell("Id poliza: "+poliza.getNroPoliza());

            documento.add(tabla);
            rutaResultado="../public/facturas/Reporte"+tiempo+".pdf";
        } catch (Exception e) {
            System.out.println(e);
        }
        documento.close();
        return rutaResultado;
    }
}