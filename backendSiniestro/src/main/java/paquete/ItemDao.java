package paquete;

import java.util.ArrayList;
import java.util.List;

import org.sql2o.Connection;

import util.Sql2oDAO;

public class ItemDao {
    
    public List<Item> getAllItems(){
        List<Item> resultados=new ArrayList<>();

        try (Connection con=Sql2oDAO.getSql2o().open()){
            String consulta="SELECT * FROM ITEMS";
            resultados=con.createQuery(consulta).executeAndFetch(Item.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultados;
    }

    public boolean addItem(Item item){
        boolean verdad=false;

        try (Connection con=Sql2oDAO.getSql2o().open()){
            String insercion="INSERT INTO ITEMS VALUES(NULL,'"+item.getTipo()+"','"+item.getDescripcion()+"', "+item.getValor_mensual()+","+item.getCobertura()+")";
            con.createQuery(insercion).executeUpdate();
            verdad=true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return verdad;
    }

    public List<Item> getItems(String tipo){
        List<Item> resultados=new ArrayList<>();
        try (Connection con=Sql2oDAO.getSql2o().open()){
            String consulta="SELECT * FROM ITEMS WHERE TIPO='"+tipo+"'";
            resultados=con.createQuery(consulta).executeAndFetch(Item.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        return resultados;
    }

    public boolean eliminarItem(int idItem){
        boolean verdad=false;
        

        try (Connection con=Sql2oDAO.getSql2o().open()){
            String sql="DELETE FROM ITEMS WHERE CODIGO="+idItem;
            con.createQuery(sql).executeUpdate();
            verdad=true;
        } catch (Exception e) {
            System.out.println(e);
        }
        
        return verdad;
    }

    public boolean actualizarItem(Item item){
        boolean verdad=false;
        String eliminar="DELETE FROM ITEMS WHERE CODIGO="+item.getCodigo();
        String insertar="INSERT INTO ITEMS VALUES ("+item.getCodigo()+",'"+item.getTipo()+"','"+item.getDescripcion()+"',"+item.getValor_mensual()+","+item.getCobertura()+")";
        try (Connection con =Sql2oDAO.getSql2o().open()){
            con.createQuery(eliminar).executeUpdate();
            con.createQuery(insertar).executeUpdate();
            verdad=true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return verdad;
    }

    public Item getItem(int idItem){
        Item resultado=new Item();
        String sql="SELECT * FROM ITEMS WHERE CODIGO="+idItem;
        try (Connection con=Sql2oDAO.getSql2o().open()){
            resultado=con.createQuery(sql).executeAndFetchFirst(Item.class);
        } catch (Exception e) {
            System.out.println(e);
        }        
        return resultado;
    }

    public List<String> getCategorias(){
        String consulta="SELECT column_type FROM information_schema.columns WHERE table_name ='SEGURO' AND column_name = 'TIPO'";
        List<String>resultados=new ArrayList<>();
        try (Connection con =Sql2oDAO.getSql2o().open()){
            resultados=con.createQuery(consulta)
                    .executeAndFetch(String.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        if(resultados.size()==1){
            String resultadoLimpio=_limpiar(resultados.get(0));
            String[] aux=resultadoLimpio.split(",");
            resultados.clear();
            for (String var : aux) {
                resultados.add(var);
            }
        }
    
        return resultados;
    }

    private String _limpiar(String palabra){
        String str1="enum(";
        String str2="'";
        String str3=")";
        String resultado="";
        resultado=palabra.replace(str1, "");
        resultado=resultado.replace(str2, "");
        resultado=resultado.replace(str3, "");

        return resultado;
    }
}