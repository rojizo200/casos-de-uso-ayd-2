package paquete;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class SeguroAuto {
    private List<Item> items;
    private Automovil auto;
    private Cliente cliente;
    private float costoMensual;
    private float valorAuto;
    private static SeguroAuto seguro;

    private SeguroAuto(){
        this.items=new ArrayList<Item>();
    }

    public static SeguroAuto getSeguroAuto(){
        if(seguro==null){
            seguro=new SeguroAuto();
        }
        return seguro;
    }

    public void addItem(Item item){
        this.items.add(item);
    }
}