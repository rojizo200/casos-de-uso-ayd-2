package paquete;
import lombok.Data;

@Data
public class Usuario{
    private String email;
    private String contraseña;
    private String dni;

    private static Usuario usuario;

    private Usuario(){
        this.email="";
        this.contraseña="";
        this.dni="";
    }

    public static Usuario getUsuarioSingleton(){
        if(usuario == null){
            usuario=new Usuario();
        }
        return usuario;
    }
}