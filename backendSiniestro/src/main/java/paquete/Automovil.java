package paquete;
import lombok.Data;

@Data
public class Automovil{
    private String nroChasis;
    private String patente;
    private String marca;
    private String modelo;
    private float valor;
    private int gnc;
    private int oKm;

    public Automovil(){
        this.nroChasis="";
        this.patente="";
        this.marca="";
        this.modelo="";
        this.valor=0;
        this.gnc=0;
        this.oKm=0;
    }
}