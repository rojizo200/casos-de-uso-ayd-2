package paquete;
import lombok.Data;


@Data
public class NumeroEmergencia{
    private String numero;
    private String descripcion;

    public NumeroEmergencia(){
        this.numero="";
        this.descripcion="";
    }

    public NumeroEmergencia(String numero,String descripcion){
        this.numero=numero;
        this.descripcion=descripcion;
    }
}