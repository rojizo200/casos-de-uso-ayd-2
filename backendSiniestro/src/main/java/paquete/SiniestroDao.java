package paquete;

import java.util.ArrayList;

import org.sql2o.Connection;

import util.Sql2oDAO;

public class SiniestroDao{
    public boolean subirSiniestro(String fecha,String latitud,String longitud,String idSeguro,ArrayList<String> fotos){
        //ESTE USER YA TIENE TODOS LOS VALORES DEL QUE INICIO SESION
        boolean verdad=false;
        float lat=Float.parseFloat(latitud);
        float longi=Float.parseFloat(longitud);
        Usuario user=Usuario.getUsuarioSingleton();
        String dni=user.getDni();
        String sentenciaSQL="INSERT INTO SINIESTRO VALUES(NULL,"+lat+","+longi+",'"+fecha+"','"+idSeguro+"','"+dni+"')";
        String selectId="SELECT MAX(IDSINIESTRO) FROM SINIESTRO";
        int idSiniestro=0;
        try (Connection con =Sql2oDAO.getSql2o().open()){
            con.createQuery(sentenciaSQL).executeUpdate();
            idSiniestro=con.createQuery(selectId).executeAndFetchFirst(Integer.class);
            for (String foto : fotos) {
                String fotosxSiniestro="INSERT INTO FOTOSXSINIESTRO VALUES('"+foto+"',"+idSiniestro+")";
                con.createQuery(fotosxSiniestro).executeUpdate();
            }
            verdad=true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return verdad;
    }
}