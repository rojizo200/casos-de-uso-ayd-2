package paquete;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.debug.DebugScreen.enableDebugScreen;

import spark.Spark;

public final class App {

    public static void main(String[] args) {
       Spark.staticFileLocation("/public/");
    
        enableDebugScreen();
        get("/api/login",ControladorGeneral.loginApp);
        get("/api/getIdSeguro",ControladorGeneral.getIdSeguro);
        get("/api/numerosEmergencia",ControladorGeneral.numerosEmergencia);
        post("/api/subirSiniestro",ControladorGeneral.subirSiniestro);

        get("/api/loginAdmin",ControladorWeb.loginAdmin);
        post("/api/loginAdmin",ControladorWeb.loginAdmin);
        
        get("/api/admin/index",ControladorAdmin.getIndex);
        get("/api/admin/addItem",ControladorAdmin.addItem);
        post("/api/admin/addItem",ControladorAdmin.addItem);
        get("/api/admin/hogar",ControladorAdmin.getItemsHogar);
        get("/api/admin/persona",ControladorAdmin.getItemsPersona);
        get("/api/admin/automovil",ControladorAdmin.getItemsAutomovil);
        get("/api/admin/eliminarItem",ControladorAdmin.eliminarItem);
        get("/api/admin/modificarItem",ControladorAdmin.modificarItem);
        post("/api/admin/modificarItem",ControladorAdmin.modificarItem);

        get("/api/index",ControladorNavegacion.getIndex);
        get("/api/loginWeb",ControladorNavegacion.login);
        post("/api/loginWeb",ControladorNavegacion.login);
        get("/api/registro",ControladorNavegacion.registro);
        post("/api/registro",ControladorNavegacion.registro);
        get("/api/logoutWeb",ControladorNavegacion.logout);
        
        get("/api/cotizar",ControladorWeb.cotizar);
        get("/api/cotizacionAuto",ControladorWeb.cotizarAuto);
        post("/api/calcularAuto",ControladorWeb.calcularAuto);
        post("/api/calcularHogar",ControladorWeb.calcularHogar);
        get("/api/cotizacionHogar",ControladorWeb.cotizarHogar);
        get("/api/cotizacionPersona",ControladorWeb.cotizarPersona);
        post("/api/calcularPersona",ControladorWeb.calcularPersona);
        get("/api/seguroPersona",ControladorWeb.contratSeguroPersona);
        post("/api/seguroPersona",ControladorWeb.contratSeguroPersona);
        get("/api/seguroAutomovil",ControladorWeb.contratSeguroAutomovil);
        post("/api/seguroAutomovil",ControladorWeb.contratSeguroAutomovil);
        get("/api/seguroHogar",ControladorWeb.contratSeguroHogar);
        post("/api/seguroHogar",ControladorWeb.contratSeguroHogar);
        post("/api/confirmarAutomovil",ControladorWeb.confirmarAutomovil);
        post("/api/confirmarHogar",ControladorWeb.confirmarHogar);
        post("/api/confirmarPersona",ControladorWeb.confirmarPersona);
    }
}
