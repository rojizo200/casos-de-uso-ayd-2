package paquete;
import lombok.Data;

@Data
public class Cliente{
    private String dni;
    private String nombre;
    private String apellido;
    private String telefono;
    private String calle;
    private String numero;
    private String fecNacimiento;   //lo tengo que insertar como date
    private String email;
    private String contraseña;
    private boolean comercial;
    private String cuit;
    private String localizacion_localidad;
    private String localizacion_provincia;

    private static Cliente cliente;

    public static Cliente getCliente(){
        if(cliente==null){
            cliente =new Cliente();
        }
        return cliente;
    }

    private Cliente(){
        this.dni="";
        this.nombre="";
        this.apellido="";
        this.telefono="";
        this.calle="";
        this.numero="";
        this.fecNacimiento="";
        this.email="";
        this.contraseña="";
        this.comercial=false;
        this.cuit="";
        this.localizacion_localidad="";
        this.localizacion_provincia="";
    }
}