package paquete;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class SeguroHogar {
    private List<Item> items;
    private Hogar hogar;
    private Cliente cliente;
    private float costoMensual;
    private float valorHogar;
    private static SeguroHogar seguro;

    private SeguroHogar(){
        this.items=new ArrayList<Item>();
    }

    public static SeguroHogar getSeguroHogar(){
        if(seguro==null){
           seguro=new SeguroHogar();
        }
        return seguro;
    }

    public void addItem(Item item){
        this.items.add(item);
    }
}