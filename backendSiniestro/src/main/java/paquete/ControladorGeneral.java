package paquete;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;

import spark.Request;
import spark.Response;
import spark.Route;



public class ControladorGeneral{

    public static Route loginApp=(Request req, Response res)->{
        Usuario user=Usuario.getUsuarioSingleton();
        UsuarioDao uDao=new UsuarioDao();

        if (req.queryParams("usuario")!=null && req.queryParams("contraseña")!=null) {
            user.setEmail(req.queryParams("usuario"));
            user.setContraseña(req.queryParams("contraseña"));
            
            List<Usuario> resultados=uDao.loginApp(user);
            if(resultados.size()>0){
                System.out.println("Ingreso");
                user.setDni(resultados.get(0).getDni());
                for (Usuario aux : resultados) {
                    System.out.println(aux.getContraseña());
                    System.out.println(aux.getEmail());
                    System.out.println(aux.getDni());
                }
                return true;
            }
        }
        return false;
    };

    public static Route getIdSeguro=(Request req,Response res)->{
        Usuario user=Usuario.getUsuarioSingleton();
        UsuarioDao uDao=new UsuarioDao();
        List<Integer> idsSeguros=uDao.getIds(user);

        ObjectMapper mapper=new ObjectMapper();
        String jsonStr=mapper.writeValueAsString(idsSeguros);
        System.out.println(jsonStr);
        return jsonStr;
    };

    public static Route numerosEmergencia=(Request req, Response res)->{
        EmergenciaDao eDao=new EmergenciaDao();
        List<NumeroEmergencia> numeros=eDao.getNumeros();

        ObjectMapper mapper=new ObjectMapper();
        String jsonStr=mapper.writeValueAsString(numeros);
        System.out.println(jsonStr);
        return jsonStr;
    };
    
    //EN ESTE METODO RECIBO UN MULTIPART QUE CONTIENE UN JSON Y LAS FOTOGRAFIAS DEL SINIESTRO
    public static Route subirSiniestro=(Request req,Response res)->{
        System.out.println("Me llamaron");
        String fecha="";
        String latitud="";
        String longitud="";
        String idSeguro="";
        ArrayList<String> fotosXSiniestro=new ArrayList();
        
        File uploadDir = new File("upload");
        uploadDir.mkdir(); 

        req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/temp"));
        fecha=IOUtils.toString(req.raw().getPart("fecha").getInputStream());
        latitud=IOUtils.toString(req.raw().getPart("latitud").getInputStream());
        longitud=IOUtils.toString(req.raw().getPart("longitud").getInputStream());
        idSeguro=IOUtils.toString(req.raw().getPart("idSeguro").getInputStream());

        for (Part parte : req.raw().getParts()) {
            if(parte.getName().equals("uploaded_file")){
                try {
                    //HAGO ESTO PARA ASEGURARME DE QUE SEA UNICO
                    String tiempo=Long.toString(new Date().getTime());
                    Path tempFile =Files.createTempFile(uploadDir.toPath(),tiempo,"");
                    InputStream input=parte.getInputStream();
                    Files.copy(input, tempFile,StandardCopyOption.REPLACE_EXISTING);
                    fotosXSiniestro.add(tiempo);
                } catch (Exception e) {
                    System.out.println(e);
                    return false;
                }
            }
        }
        SiniestroDao sDao=new SiniestroDao();
        boolean verdad=false;
        verdad=sDao.subirSiniestro(fecha,latitud,longitud,idSeguro,fotosXSiniestro);
        return verdad;
    };
}