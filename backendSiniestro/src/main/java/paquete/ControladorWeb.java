package paquete;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;



public class ControladorWeb {
    private static String rutaAnterior = "";

    
    public static String getRutaAnterior(){
        return rutaAnterior;
    }


    public static Route loginAdmin = (Request req,Response res)->{
        HashMap<String,Object> model=new HashMap<>();
        model.put("admin",true);
        Usuario user=Usuario.getUsuarioSingleton();
        UsuarioDao uDao=new UsuarioDao();
        if(req.queryParams("email") != null && req.queryParams("contraseña") != null){
            user.setEmail(req.queryParams("email"));
            user.setContraseña(req.queryParams("contraseña"));
            boolean verdad=uDao.loginAdmin(user);
            if(verdad){
                req.session(true);                    
                req.session().attribute("email", user.getEmail() );
                req.session().attribute("admin",true);
                res.redirect("admin/index");
                return new VelocityTemplateEngine().render(new ModelAndView(model, "/resources/templates/admin/index.vtl"));
            }else{
                //INGRESO ATRIBUTOS PERO SON INCORRECTOS
                model.put("error", "Usuario o contraseña incorrectos");
                return new VelocityTemplateEngine().render(new ModelAndView(model,"/resources/templates/login.vtl"));
            }
        }else{
            return new VelocityTemplateEngine().render(new ModelAndView(model,"/resources/templates/login.vtl"));
        }
    };

    // SOLO NOS MUESTRA LAS OPCIONES A COTIZAR
    public static Route cotizar = (Request req, Response res) -> {
        HashMap<String, String> model = new HashMap<String,String>();
        model.put("template", "./resources/templates/cotizar.vtl");
        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };
    // NOS DA A ELEGIR LOS ITEMS Y EL VALOR DEL AUTO, TAMBIEN EL BOTON DE CALCULAR
    public static Route cotizarAuto = (Request req, Response res) -> {
        HashMap<String,Object> model = new HashMap<String,Object>();
        AutomovilDao aDao = new AutomovilDao();
        List<Item> items = aDao.getItemsAutomovil();
        model.put("items", items);
        model.put("template", "./resources/templates/cotizacionAuto.vtl");
        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };
    // NOS DEVUELVE LA CUOTA MENSUAL
    public static Route calcularAuto = (Request req, Response res) -> {
        HashMap<String,Object> model = new HashMap<String,Object>();
        SeguroAuto seguroAuto = SeguroAuto.getSeguroAuto();
        AutomovilDao aDao = new AutomovilDao();
        float costoMensual = aDao.calcular(req.queryParams(), Float.parseFloat(req.queryParams("valorAuto")));
        seguroAuto.setCostoMensual(costoMensual);
        seguroAuto.setValorAuto(Float.parseFloat(req.queryParams("valorAuto")));
        aDao.addItems(seguroAuto, req.queryParams());
        model.put("costo", costoMensual);
        model.put("items", seguroAuto.getItems());
        model.put("tipoSeguro", "Automovil");
        model.put("template", "./resources/templates/resultadoCotizacion.vtl");
        // DEVUELVE LA PAGINA DONDE SE MUESTRAN LOS RESULTADOS
        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };
    // NOS DA A ELEGIR LOS ITEMS Y EL VALOR DEL AUTO, TAMBIEN EL BOTON DE CALCULAR
    public static Route cotizarHogar = (Request req, Response res) -> {
        HogarDao hDao = new HogarDao();
        List<String> tipos = hDao.getTiposHogar();
        List<Item> items = hDao.getItemsHogar();

        HashMap<String,Object> model = new HashMap<String,Object>();

        model.put("tipos", tipos);
        model.put("items", items);
        model.put("template", "./resources/templates/cotizacionHogar.vtl");
        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };

    public static Route calcularHogar = (Request req, Response res) -> {
        HashMap<String,Object> model = new HashMap<>();
        SeguroHogar seguroHogar = SeguroHogar.getSeguroHogar();
        HogarDao hDao = new HogarDao();
        float costoMensual = hDao.calcular(req.queryParams(), Float.parseFloat(req.queryParams("valorHogar")));
        seguroHogar.setCostoMensual(costoMensual);
        seguroHogar.setValorHogar(Float.parseFloat(req.queryParams("valorHogar")));
        hDao.addItems(seguroHogar, req.queryParams());
        System.out.println(costoMensual);
        model.put("costo", costoMensual);
        model.put("items", seguroHogar.getItems());
        model.put("tipoSeguro", "Hogar");
        model.put("template", "./resources/templates/resultadoCotizacion.vtl");
        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };

    public static Route cotizarPersona = (Request req, Response res) -> {
        HashMap<String,Object> model = new HashMap<String,Object>();
        PersonaDao pDao = new PersonaDao();
        List<Item> items = pDao.getItemsPersona();
        List<String> categorias = pDao.getCategorias();
        model.put("items", items);
        model.put("categorias", categorias);
        model.put("template", "./resources/templates/cotizacionPersona.vtl");
        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };

    public static Route calcularPersona = (Request req, Response res) -> {
        HashMap<String,Object> model = new HashMap<>();
        PersonaDao pDao = new PersonaDao();
        SeguroPersona seguro = SeguroPersona.getSeguroPersona();
        float costoMensual = pDao.calcular(req.queryParams());
        seguro.setCostoMensual(costoMensual);
        pDao.addItems(seguro, req.queryParams());
        System.out.println(costoMensual);
        model.put("costo", costoMensual);
        model.put("items", seguro.getItems());
        model.put("tipoSeguro", "Persona");
        model.put("template", "./resources/templates/resultadoCotizacion.vtl");

        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };

    public static Route contratSeguroPersona = (Request req, Response res) -> {
        HashMap<String,Object> model = new HashMap<>();
        if (req.session().attribute("email") == null) {
            model.put("template", "./resources/templates/login.vtl");
            rutaAnterior = "/api/seguroPersona";
        } else {
            if (req.queryParams().isEmpty()) {
                GeneralDao gDao = new GeneralDao();
                PersonaDao pDao = new PersonaDao();
                List<String> estadosCiviles = pDao.getEstadoCivil();
                List<String> localidades = gDao.getLocalidades();
                List<String> provincias = gDao.getProvincias();
                model.put("estadosCiviles", estadosCiviles);
                model.put("provincias", provincias);
                model.put("localidades", localidades);
                model.put("template", "./resources/templates/contratarPersonal.vtl");
                return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
            } else {
                Cliente cliente = Cliente.getCliente();
                Persona persona = new Persona();
                SeguroPersona seguro = SeguroPersona.getSeguroPersona();
                //inicializarCliente(req, cliente);

                persona.setDni(req.queryParams("Pdni"));
                persona.setNombre(req.queryParams("Pnombre"));
                persona.setApellido(req.queryParams("Papellido"));
                persona.setFecNacimiento(req.queryParams("PfecNacimiento"));
                System.out.println(req.queryParams("PestadoCivil"));
                persona.setEstadoCivil(req.queryParams("PestadoCivil"));
                persona.setProfesion(req.queryParams("Pprofesion"));

                seguro.setCliente(cliente);
                seguro.setPersona(persona);
                model.put("seguro", seguro);
                model.put("tipo", "Persona");
                model.put("template", "./resources/templates/pantallaPago.vtl");

            }
        }
        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };

    public static Route contratSeguroHogar = (Request req, Response res) -> {

        HashMap<String,Object> model = new HashMap<>();
        if (req.session().attribute("email") == null) {
            model.put("template", "./resources/templates/login.vtl");
            rutaAnterior = "/api/seguroHogar";
        } else {
            if (req.queryParams().isEmpty()) {
                GeneralDao gDao = new GeneralDao();
                HogarDao hDao = new HogarDao();
                List<String> tiposHogares = hDao.getTiposHogar();
                List<String> localidades = gDao.getLocalidades();
                List<String> provincias = gDao.getProvincias();
                model.put("tipos", tiposHogares);
                model.put("provincias", provincias);
                model.put("localidades", localidades);
                model.put("template", "./resources/templates/contratarHogar.vtl");
                return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
            } else {
                Cliente cliente = Cliente.getCliente();
                Hogar hogar = new Hogar();
                SeguroHogar seguro = SeguroHogar.getSeguroHogar();

                //inicializarCliente(req, cliente);

                hogar.setCalle(req.queryParams("Hcalle"));
                hogar.setNumero(Integer.parseInt(req.queryParams("Hnumero")));
                hogar.setCodigoPostal(Integer.parseInt(req.queryParams("HcodigoPostal")));
                hogar.setTipo(req.queryParams("Htipo"));
                hogar.setProvincia(req.queryParams("provincia"));
                hogar.setLocalidad(req.queryParams("localidad"));

                seguro.setCliente(cliente);
                seguro.setHogar(hogar);
                model.put("seguro", seguro);
                model.put("tipo", "Hogar");

                model.put("template", "./resources/templates/pantallaPago.vtl");
            }
        }
        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };

    public static Route contratSeguroAutomovil = (Request req, Response res) -> {
        HashMap<String,Object> model = new HashMap<>();
        // SI NO INICIO SESION LO MANDO A LOGUEARSE
        if (req.session().attribute("email") == null) {
            model.put("template", "./resources/templates/login.vtl");
            rutaAnterior = "/api/seguroAutomovil";
        } else {
            if (req.queryParams().isEmpty()) {
                GeneralDao gDao = new GeneralDao();
                List<String> localidades = gDao.getLocalidades();
                List<String> provincias = gDao.getProvincias();
                model.put("provincias", provincias);
                model.put("localidades", localidades);
                model.put("template", "./resources/templates/contratarAuto.vtl");
            } else {
                Cliente cliente = Cliente.getCliente();
                Automovil auto = new Automovil();
                SeguroAuto seguro = SeguroAuto.getSeguroAuto();
                //inicializarCliente(req, cliente);

                auto.setNroChasis(req.queryParams("AnroChasis"));
                auto.setPatente(req.queryParams("Apatente"));
                auto.setMarca(req.queryParams("Amarca"));
                auto.setModelo(req.queryParams("Amodelo"));
                if (req.queryParams("Agnc") != null) {
                    auto.setGnc(1);
                }
                if (req.queryParams("AoKm") != null) {
                    auto.setOKm(1);
                }
                seguro.setCliente(cliente);
                seguro.setAuto(auto);
                model.put("seguro", seguro);
                model.put("tipo", "Automovil");
                model.put("template", "./resources/templates/pantallaPago.vtl");

            }
        }
        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };

    
    //ACA SE CARGA EN LA BASE DE DATOS Y SE GENERA LA FACTURA
    public static Route confirmarAutomovil = (Request req, Response res) -> {
        HashMap<String,Object> model=new HashMap<>();
        String fecInicio = req.queryParams("fecInicio");
        String fecFinal = req.queryParams("fecFinal");
        AutomovilDao aDao=new AutomovilDao();
        boolean verdad=aDao.cargarSeguroAuto(fecInicio, fecFinal);
        
        //obtengo el ultimo id guardado y lo mando a generar factura
        int idSeguro=aDao.getLastId();
        String rutaDescarga=PolizaDao.generarFactura(idSeguro,"Hogar");     //aca seria conveniente pasarle el tipo de seguro
        Poliza poliza=PolizaDao.getPoliza();
        model.put("ruta",rutaDescarga);
        model.put("poliza",poliza);
        model.put("nombreCliente",PolizaDao.getNombreCliente());
        model.put("tipo","Automovil");
        model.put("fecha",java.time.LocalDate.now());
        model.put("template","./resources/templates/factura.vtl");

        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };
    //ACA SE CARGA EN LA BASE DE DATOS Y SE GENERA LA FACTURA
    public static Route confirmarHogar = (Request req, Response res) -> {
        HashMap<String,Object> model=new HashMap<>();
        String fecInicio = req.queryParams("fecInicio");
        String fecFinal = req.queryParams("fecFinal");
        HogarDao hDao = new HogarDao();
        boolean verdad = hDao.cargarSeguroHogar(fecInicio, fecFinal);
        //obtengo el ultimo id guardado y lo mando a generar factura
        int idSeguro=hDao.getLastId();
        String rutaDescarga=PolizaDao.generarFactura(idSeguro,"Hogar");     //aca seria conveniente pasarle el tipo de seguro
        Poliza poliza=PolizaDao.getPoliza();
        model.put("ruta",rutaDescarga);
        model.put("poliza",poliza);
        model.put("nombreCliente",PolizaDao.getNombreCliente());
        model.put("tipo","Hogar");
        model.put("fecha",java.time.LocalDate.now());
        model.put("template","./resources/templates/factura.vtl");

        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };
    //ACA SE CARGA EN LA BASE DE DATOS Y SE GENERA LA FACTURA
    public static Route confirmarPersona = (Request req, Response res) -> {
        HashMap<String,Object> model=new HashMap<>();
        String fecInicio = req.queryParams("fecInicio");
        String fecFinal = req.queryParams("fecFinal");
        PersonaDao pDao=new PersonaDao();
        boolean verdad=pDao.cargarSeguroPersona(fecInicio, fecFinal);
        //obtengo el ultimo id guardado y lo mando a generar factura
        int idSeguro=pDao.getLastId();
        String rutaDescarga=PolizaDao.generarFactura(idSeguro,"Persona");     //aca seria conveniente pasarle el tipo de seguro
        Poliza poliza=PolizaDao.getPoliza();
        model.put("ruta",rutaDescarga);
        model.put("poliza",poliza);
        model.put("nombreCliente",PolizaDao.getNombreCliente());
        model.put("tipo","Persona");
        model.put("fecha",java.time.LocalDate.now());
        model.put("template","./resources/templates/factura.vtl");

        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };

}