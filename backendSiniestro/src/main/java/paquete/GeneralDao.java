package paquete;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.sql2o.Connection;

import lombok.Data;
import util.Sql2oDAO;





@Data
public class GeneralDao{

    public List<String> getLocalidades(){
        String consulta="SELECT DISTINCT LOCALIDAD FROM LOCALIZACION";
        List<String> resultados=new ArrayList<>();
        try (Connection con =Sql2oDAO.getSql2o().open()){
            resultados=con.createQuery(consulta)
                    .executeAndFetch(String.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultados;
    }

    public List<String> getProvincias(){
        String consulta="SELECT DISTINCT PROVINCIA FROM LOCALIZACION";
        List<String> resultados=new ArrayList<>();
        try (Connection con =Sql2oDAO.getSql2o().open()){
            resultados=con.createQuery(consulta)
                    .executeAndFetch(String.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultados;
    }

}