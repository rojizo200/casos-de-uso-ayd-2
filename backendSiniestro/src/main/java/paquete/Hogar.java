package paquete;

import lombok.Data;

@Data
public class Hogar{
    private String calle;
    private int numero;
    private int codigoPostal;
    private String tipo;
    private float valor;
    private String provincia;
    private String localidad;

    public Hogar(){
        this.calle="";
        this.numero=0;
        this.codigoPostal=0;
        this.tipo="";
        this.valor=0;
        this.provincia="";
        this.localidad="";
    }
}