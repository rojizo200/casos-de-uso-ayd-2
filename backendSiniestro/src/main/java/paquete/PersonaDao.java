package paquete;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.sql2o.Connection;

import util.Sql2oDAO;

public class PersonaDao {

    public List<Item> getItemsPersona(){
        String sentenciaSQL="SELECT DESCRIPCION,VALOR_MENSUAL,COBERTURA FROM ITEMS WHERE TIPO='PERSONA'";
        List<Item> resultados=new ArrayList<Item>();
        try (Connection con =Sql2oDAO.getSql2o().open()){
            resultados=con.createQuery(sentenciaSQL)
                    .executeAndFetch(Item.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultados;
    }

    public List<String> getCategorias(){
        String consulta="SELECT column_type FROM information_schema.columns WHERE table_name ='SEGUROPERSONA' AND column_name = 'CATEGORIA'";
        List<String>resultados=new ArrayList<>();
        try (Connection con =Sql2oDAO.getSql2o().open()){
            resultados=con.createQuery(consulta)
                    .executeAndFetch(String.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        if(resultados.size()==1){
            String resultadoLimpio=_limpiar(resultados.get(0));
            String[] aux=resultadoLimpio.split(",");
            resultados.clear();
            for (String var : aux) {
                resultados.add(var);
            }
        }
    
        return resultados;
    }

    String _limpiar(String palabra){
        String str1="enum(";
        String str2="'";
        String str3=")";
        String resultado="";
        resultado=palabra.replace(str1, "");
        resultado=resultado.replace(str2, "");
        resultado=resultado.replace(str3, "");

        return resultado;
    }

    public float calcular(Set<String> parametros){
        float valorMensual=0;
        float costo;
        float cobertura;
        for (String var : parametros) {
            if(!var.equals("categorias")){
                costo=buscarCosto(var);
                valorMensual=valorMensual+costo;
            }
        }
        return valorMensual;
    }

    int getLastId(){
        int resultado=-1;
        String sql="SELECT MAX(IDSEGURO) FROM SEGUROPERSONA";
        try (Connection con=Sql2oDAO.getSql2o().open()){
            resultado=con.createQuery(sql).executeAndFetchFirst(Integer.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultado;
    }

    private float buscarCosto(String item){
        String consulta="SELECT VALOR_MENSUAL FROM ITEMS WHERE DESCRIPCION="+"'"+item+"'"+"AND TIPO='PERSONA'";
        float resultado=0;
        try (Connection con =Sql2oDAO.getSql2o().open()){
            resultado=con.createQuery(consulta).executeAndFetchFirst(Float.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultado;
    }

    public void addItems(SeguroPersona seguro,Set<String> items){
        Item aux=new Item();
        for (String var : items) {
            String consulta="SELECT * FROM ITEMS WHERE DESCRIPCION="+"'"+var+"'"+"AND TIPO='PERSONA'";
            if(!var.equals("categorias")){
                try (Connection con =Sql2oDAO.getSql2o().open()){
                    aux=con.createQuery(consulta).executeAndFetchFirst(Item.class);
                } catch (Exception e) {
                    System.out.println(e);
                }
                seguro.addItem(aux);
            }
        }
    }

    List<String> getEstadoCivil(){
        String consulta="SELECT column_type FROM information_schema.columns WHERE table_name ='PERSONA' AND column_name = 'ESTADOCIVIL'";
        List<String>resultados=new ArrayList<>();
        try (Connection con =Sql2oDAO.getSql2o().open()){
            resultados=con.createQuery(consulta)
                    .executeAndFetch(String.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        if(resultados.size()==1){
            String resultadoLimpio=_limpiar(resultados.get(0));
            String[] aux=resultadoLimpio.split(",");
            resultados.clear();
            for (String var : aux) {
                resultados.add(var);
            }
        }
    
        return resultados;
    }

    boolean cargarSeguroPersona(String fecInicio,String fecFinal){
        SeguroPersona seguro=SeguroPersona.getSeguroPersona();
        Cliente cliente=seguro.getCliente();
        Persona persona=seguro.getPersona();
        int idSeguro=0;
        boolean verdad=false;
        String inserPersona="INSERT INTO PERSONA VALUES(:dni,:nombre,:apellido,:fecNacimiento,:estadoCivil,:profesion)";
        String inserSeguro="INSERT INTO SEGURO VALUES(NULL,"+cliente.getDni()+",'PERSONA',1)";
        String selectId="SELECT MAX(IDSEGURO) FROM SEGURO";
        
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            con.createQuery(inserPersona).bind(persona).executeUpdate();
            con.createQuery(inserSeguro).executeUpdate();
            idSeguro=con.createQuery(selectId).executeAndFetchFirst(Integer.class);
            String inserSeguroPersona="INSERT INTO SEGUROPERSONA VALUES("+idSeguro+",'"+persona.getDni()+"')";
            System.out.println(inserSeguroPersona);
            con.createQuery(inserSeguroPersona).executeUpdate();
            for (Item item : seguro.getItems()){
                String insertItem="INSERT INTO ITEMSXSEGURO VALUES("+idSeguro+","+item.getCodigo()+")";
                con.createQuery(insertItem).executeUpdate();
            }
            String inserPoliza="INSERT INTO POLIZA VALUES(NULL,STR_TO_DATE("+fecInicio+",'%Y-%m-%d'),STR_TO_DATE("+fecFinal+",'%Y-%m-%d'),null,"+seguro.getCostoMensual()+","+idSeguro+","+cliente.getDni()+")";
            con.createQuery(inserPoliza).executeUpdate();
            verdad=true;
       } catch(Exception e){
            System.out.println(e);
       }

        return verdad;
    }
}