package paquete;
import lombok.Data;

@Data
public class Persona{
    private String dni;
    private String nombre;
    private String apellido;
    private String fecNacimiento;       //Esto lo tengo que insertar como date
    private String estadoCivil;
    private String profesion;

    public Persona(){
        this.dni="";
        this.nombre="";
        this.apellido="";
        this.fecNacimiento="";
        this.estadoCivil="";
        this.profesion="";
    }
}