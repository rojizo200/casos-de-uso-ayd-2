package paquete;
import java.util.HashMap;
import java.util.List;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;

public class ControladorNavegacion{
    private static String rutaAnterior = "";

    public static Route getIndex = (Request req, Response res) -> {
        HashMap<String, String> model = new HashMap<String, String>();
        model.put("template", "./resources/templates/index.vtl");
        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };

    public static Route login = (Request req, Response res) -> {
        HashMap<String,Object> model = new HashMap<String,Object>();
        Usuario user = Usuario.getUsuarioSingleton();
        UsuarioDao uDAO = new UsuarioDao();
        if (req.queryParams("email") != null && req.queryParams("contraseña") != null) {
            user.setEmail(req.queryParams("email"));
            user.setContraseña(req.queryParams("contraseña"));
            //List<Usuario> resultados = uDAO.login(user);
            boolean verdad=uDAO.login(user);
            
            if (verdad) {
                req.session(true);
                req.session().attribute("email", user.getEmail());
                rutaAnterior=ControladorWeb.getRutaAnterior();
                //TENGO QUE DECLARAR UN CLIENTE ACA
                
                switch (rutaAnterior) {
                    case "/api/seguroAutomovil":
                    model.put("template", "./resources/templates/contratarAuto.vtl");
                    break;
                    
                    case "/api/seguroHogar":
                    model.put("template", "./resources/templates/contratarHogar.vtl");
                    break;
                    
                    case "/api/seguroPersona":
                    model.put("template", "./resources/templates/contratarPersona.vtl");
                    break;
                    
                    default:
                    model.put("template", "./resources/templates/index.vtl");
                    break;
                }
                res.redirect(rutaAnterior);
            } else {
                // INGRESO ATRIBUTOS PERO SON INCORRECTOS
                model.put("error", "Usuario o contraseña incorrectos");
                model.put("template", "./resources/templates/login.vtl");
            }
        } else {
            model.put("template", "./resources/templates/login.vtl");
        }
        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };

    // REGISTRO UN CLIENTE Y YA LE INICIO SESSION
    public static Route registro = (Request req, Response res) -> {
        HashMap<String,Object> model = new HashMap<>();
        if (!req.queryParams().isEmpty()) {
            System.out.println(req.queryParams());
            Cliente cliente = Cliente.getCliente();
            inicializarCliente(req, cliente);
            UsuarioDao uDao = new UsuarioDao();
            //boolean verdad = uDao.registrar(cliente);

            //if (verdad) {
                req.session(true);
                req.session().attribute("email", cliente.getEmail());
                res.redirect(rutaAnterior);
                switch (rutaAnterior) {
                case "/api/seguroAutomovil":
                    model.put("template", "./resources/templates/contratarAuto.vtl");
                    //SeguroAuto.getSeguroAuto().setCliente(cliente);
                    break;

                case "/api/seguroHogar":
                    model.put("template", "./resources/templates/contratarHogar.vtl");
                    //SeguroHogar.getSeguroHogar().setCliente(cliente);
                    break;

                case "/api/seguroPersona":
                    model.put("template", "./resources/templates/contratarPersona.vtl");
                    //SeguroPersona.getSeguroPersona().setCliente(cliente);
                    break;

                default:
                    model.put("template", "./resources/templates/index.vtl");
                    break;
                }
            //}
        } else {
            GeneralDao gDao=new GeneralDao();
            List<String> localidades=gDao.getLocalidades();
            List<String> provincias=gDao.getProvincias();
            model.put("localidades",localidades);
            model.put("provincias",provincias);
            model.put("template", "./resources/templates/registro.vtl");
        }
        return new VelocityTemplateEngine().render(new ModelAndView(model, "./resources/templates/layout.vtl"));
    };

    public static Route logout = (Request req, Response res) -> {
        req.session().removeAttribute("email");
        res.redirect("/api/index");
        return null;
    };

    private static void inicializarCliente(Request req, Cliente cliente) {
        System.out.println(req.queryParams());
        cliente.setNombre(req.queryParams("nombre"));
        cliente.setDni(req.queryParams("dni"));
        cliente.setApellido(req.queryParams("apellido"));
        cliente.setTelefono(req.queryParams("telefono"));
        cliente.setCalle(req.queryParams("calle"));
        cliente.setNumero(req.queryParams("numero"));
        cliente.setFecNacimiento(req.queryParams("fecNacimiento"));
        cliente.setLocalizacion_provincia(req.queryParams("provincia"));
        cliente.setLocalizacion_localidad(req.queryParams("localidad"));
        cliente.setEmail(req.queryParams("email"));
        cliente.setContraseña(req.queryParams("contraseña"));
        if (req.queryParams("comercial") != null) {
            cliente.setCuit(req.queryParams("cuit"));
            cliente.setComercial(true);
        }
    }
}