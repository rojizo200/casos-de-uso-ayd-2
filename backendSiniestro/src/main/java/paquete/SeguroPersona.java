package paquete;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class SeguroPersona {
    private List<Item> items;
    private Persona persona;
    private Cliente cliente;
    private float costoMensual;
    private static SeguroPersona seguro;

    private SeguroPersona(){
        this.items=new ArrayList<Item>();
    }

    public static SeguroPersona getSeguroPersona(){
        if(seguro==null){
            seguro=new SeguroPersona();
        }
        return seguro;
    }

    public void addItem(Item item){
        this.items.add(item);
    }
}