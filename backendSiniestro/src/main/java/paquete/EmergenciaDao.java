package paquete;

import java.util.List;

import org.sql2o.Connection;

import util.Sql2oDAO;

class EmergenciaDao {
    List<NumeroEmergencia> numeros;


    List<NumeroEmergencia> getNumeros(){
        String consulta="SELECT * FROM NUMEMERGENCIA";
        try (Connection con =Sql2oDAO.getSql2o().open()){
            this.numeros=con.createQuery(consulta)
                    .executeAndFetch(NumeroEmergencia.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return this.numeros;
    }
}