package paquete;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.sql2o.Connection;

import util.Sql2oDAO;

public class AutomovilDao {
    
    public List<Item> getItemsAutomovil(){
        String sentenciaSQL="SELECT DESCRIPCION,VALOR_MENSUAL,COBERTURA FROM ITEMS WHERE TIPO='AUTOMOVIL'";
        List<Item> resultados=new ArrayList<Item>();
        try (Connection con =Sql2oDAO.getSql2o().open()){
            resultados=con.createQuery(sentenciaSQL)
                    .executeAndFetch(Item.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultados;
    }

    public float calcular(Set<String> parametros,float valorAuto){
        float valorMensual=0;
        float costo;
        float cobertura;
        for (String var : parametros) {
            if(!var.equals("valorAuto")){
                costo=buscarCosto(var);
                valorMensual=valorMensual+(valorAuto*costo/100);    //DIVIDO POR 100 PORQUE LLEGA UN PORCENTAJE
            }
        }
        return valorMensual;
    }

    private float buscarCosto(String item){
        String consulta="SELECT VALOR_MENSUAL FROM ITEMS WHERE DESCRIPCION="+"'"+item+"'"+"AND TIPO='AUTOMOVIL'";
        float resultado=0;
        try (Connection con =Sql2oDAO.getSql2o().open()){
            resultado=con.createQuery(consulta).executeAndFetchFirst(Float.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultado;
    }

    int getLastId(){
        int resultado=-1;
        String sql="SELECT MAX(IDSEGURO) FROM SEGUROAUTO";
        try (Connection con=Sql2oDAO.getSql2o().open()){
            resultado=con.createQuery(sql).executeAndFetchFirst(Integer.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultado;
    }

    public void addItems(SeguroAuto seguro,Set<String> items){
        Item aux=new Item();
        for (String var : items) {
            String consulta="SELECT * FROM ITEMS WHERE DESCRIPCION="+"'"+var+"'"+"AND TIPO='AUTOMOVIL'";
            if(!var.equals("valorAuto")){
                try (Connection con =Sql2oDAO.getSql2o().open()){
                    aux=con.createQuery(consulta).executeAndFetchFirst(Item.class);
                } catch (Exception e) {
                    System.out.println(e);
                }
                seguro.addItem(aux);
            }
        }
    }

    public boolean cargarSeguroAuto(String fecInicio,String fecFinal){
        SeguroAuto seguro=SeguroAuto.getSeguroAuto();
        Cliente cliente=seguro.getCliente();
        Automovil auto=seguro.getAuto();
        int idSeguro=0;
        boolean verdad=false;
        String inserAuto="INSERT INTO AUTOMOVIL(NROCHASIS,PATENTE,MARCA,MODELO,VALOR,GNC,OKM) VALUES(:nroChasis,:patente,:marca,:modelo,:valor,:gnc,"+auto.getOKm()+")";
        String inserSeguro="INSERT INTO SEGURO VALUES(NULL,"+cliente.getDni()+",'AUTOMOVIL',1)";
        String selectId="SELECT MAX(IDSEGURO) FROM SEGURO";
        
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            con.createQuery(inserAuto).bind(auto).executeUpdate();
            con.createQuery(inserSeguro).executeUpdate();
            idSeguro=con.createQuery(selectId).executeAndFetchFirst(Integer.class);
            String inserSeguroAuto="INSERT INTO SEGUROAUTO(IDSEGURO,AUTOMOVIL_NROCHASIS,AUTOMOVIL_PATENTE) VALUES("+idSeguro+",'"+auto.getNroChasis()+"','"+auto.getPatente()+"')";
            System.out.println(inserSeguroAuto);
            con.createQuery(inserSeguroAuto).executeUpdate();
            for (Item item : seguro.getItems()){
                String insertItem="INSERT INTO ITEMSXSEGURO VALUES("+idSeguro+","+item.getCodigo()+")";
                con.createQuery(insertItem).executeUpdate();
            }
            String inserPoliza="INSERT INTO POLIZA VALUES(NULL,STR_TO_DATE("+fecInicio+",'%Y-%m-%d'),STR_TO_DATE("+fecFinal+",'%Y-%m-%d'),null,"+seguro.getCostoMensual()+","+idSeguro+","+cliente.getDni()+")";
            System.out.println(inserPoliza);
            con.createQuery(inserPoliza).executeUpdate();
            verdad=true;
       } catch(Exception e){
            System.out.println(e);
       }
        return verdad;
    }


    /*String sentenciaSQL="INSERT INTO USUARIO(NOMBRE,APELLIDO,DOCUMENTO,EMAIL,CONTRASEÑA)VALUES(:nombre,:apellido,:documento,:email,:contraseña)";
        boolean verdad=false;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            con.createQuery(sentenciaSQL).bind(user).executeUpdate();
            verdad=true;
       } catch(Exception e){
            System.out.println(e);
       }*/
}