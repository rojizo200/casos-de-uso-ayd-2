package paquete;

import java.util.ArrayList;
import java.util.List;

import org.sql2o.Connection;

import util.Sql2oDAO;

public class UsuarioDao{
    private List<Usuario> usuarios;

    public List<Usuario> loginApp(Usuario user){
        String sentenciaSQL="SELECT EMAIL,CONTRASEÑA,DNI FROM CLIENTE WHERE EMAIL= :email AND CONTRASEÑA= :contraseña";
        try (Connection con =Sql2oDAO.getSql2o().open()){
            String email=user.getEmail();
            String contraseña=user.getContraseña();
            this.usuarios=con.createQuery(sentenciaSQL)
                    .addParameter("email",email)
                    .addParameter("contraseña", contraseña)
                    .executeAndFetch(Usuario.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return this.usuarios;
    }

    public boolean login(Usuario user){
        boolean verdad=false;
        Cliente cliente=Cliente.getCliente();
        String sentenciaSql="SELECT * FROM CLIENTE WHERE EMAIL='"+user.getEmail()+"' AND CONTRASEÑA= '"+user.getContraseña()+"'";
        try (Connection con =Sql2oDAO.getSql2o().open()){
            Cliente aux=con.createQuery(sentenciaSql).executeAndFetchFirst(Cliente.class);
            inicializarCliente(cliente,aux);
            verdad=true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return verdad;
    }

    public boolean registrar(Cliente cliente){
        boolean verdad=false;
        String inserCliente="INSERT INTO CLIENTE VALUES(:dni,:nombre,:apellido,:telefono,:calle,:numero,STR_TO_DATE(:fecNacimiento,'%Y-%m-%d'),:email,:contraseña,:comercial,:cuit,:localizacion_localidad,:localizacion_provincia)";
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            con.createQuery(inserCliente).bind(cliente).executeUpdate();
            verdad=true;
       } catch(Exception e){
            System.out.println(e);
       }
        return verdad;
    }

    public boolean loginAdmin(Usuario user){
        boolean verdad=false;
        String usuario=user.getEmail();
        String contrasenia=user.getContraseña();
        String consulta="SELECT EMAIL FROM ADMINISTRADORES WHERE EMAIL='"+usuario+"' AND CONTRASEÑA='"+contrasenia+"'";
        try (Connection con=Sql2oDAO.getSql2o().open()){
            String email=con.createQuery(consulta).executeAndFetchFirst(String.class);
            if(email!=null){
                verdad=true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        
        
        return verdad;
    }

    public List<Integer> getIds(Usuario user){
        String sentenciaSQL = "SELECT IDSEGURO FROM SEGURO WHERE DNI_CLIENTE= :dni";
        List<Integer> resultados=new ArrayList<Integer>();
        try (Connection con =Sql2oDAO.getSql2o().open()){
            String dni=user.getDni();
            resultados=con.createQuery(sentenciaSQL)
                    .addParameter("dni",dni)
                    .executeAndFetch(Integer.class);
        } catch (Exception e) {
            System.out.println(e);
        }

        return resultados;
    }

    void inicializarCliente(Cliente cliente,Cliente aux){
        cliente.setNombre(aux.getNombre());
        cliente.setDni(aux.getDni());
        cliente.setApellido(aux.getApellido());
        cliente.setTelefono(aux.getTelefono());
        cliente.setCalle(aux.getCalle());
        cliente.setNumero(aux.getNumero());
        cliente.setFecNacimiento(aux.getFecNacimiento());
        cliente.setLocalizacion_provincia(aux.getLocalizacion_provincia());
        cliente.setLocalizacion_localidad(aux.getLocalizacion_localidad());
        cliente.setEmail(aux.getEmail());
        cliente.setContraseña(aux.getContraseña());
        if (aux.getCuit() != null) {
            cliente.setCuit(aux.getCuit());
            cliente.setComercial(true);
        }
    }
}