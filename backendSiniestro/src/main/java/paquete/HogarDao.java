package paquete;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.sql2o.Connection;

import util.Sql2oDAO;

public class HogarDao {

    public List<String> getTiposHogar(){
        //ESTO NOS DEVUELVE ENUM('CASA','DEPARTAMENTO')
        String sentenciaSQL="SELECT column_type FROM information_schema.columns WHERE table_name ='HOGAR' AND column_name = 'TIPO'";
        List<String>resultados=new ArrayList<>();
        try (Connection con =Sql2oDAO.getSql2o().open()){
            resultados=con.createQuery(sentenciaSQL)
                    .executeAndFetch(String.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        
        if(resultados.size()==1){
            String resultadoLimpio=_limpiar(resultados.get(0));
            String[] aux=resultadoLimpio.split(",");
            resultados.clear();
            for (String var : aux) {
                resultados.add(var);
            }
        }
    
        return resultados;

    }
    //CREO QUE ESTO SE PODRIA HABER HECHO CON UNA EXPRESION REGULAR PERO NO ME ACUERDO COMO ERA
    String _limpiar(String palabra){
        String str1="enum(";
        String str2="'";
        String str3=")";
        String resultado="";
        resultado=palabra.replace(str1, "");
        resultado=resultado.replace(str2, "");
        resultado=resultado.replace(str3, "");

        return resultado;
    }

    public List<Item> getItemsHogar(){
        String sentenciaSQL="SELECT DESCRIPCION,VALOR_MENSUAL,COBERTURA FROM ITEMS WHERE TIPO='HOGAR'";
        List<Item> resultados=new ArrayList<Item>();
        try (Connection con =Sql2oDAO.getSql2o().open()){
            resultados=con.createQuery(sentenciaSQL)
                    .executeAndFetch(Item.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultados;
    }

    float calcular(Set<String> parametros,float valorHogar){
        float valorMensual=0;
        float costo;
        for (String var : parametros) {
            if(!var.equals("valorHogar")){
                costo=buscarCosto(var);
                valorMensual=valorMensual+(valorHogar*costo/100);
            }
        }
        
        return valorMensual;
    }

    private float buscarCosto(String item){
        String consulta="SELECT VALOR_MENSUAL FROM ITEMS WHERE DESCRIPCION="+"'"+item+"'"+"AND TIPO='HOGAR'";
        float resultado=0;
        try (Connection con =Sql2oDAO.getSql2o().open()){
            resultado=con.createQuery(consulta).executeAndFetchFirst(Float.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultado;
    }

    public void addItems(SeguroHogar seguro,Set<String> items){
        Item aux=new Item();
        for (String var : items) {
            String consulta="SELECT * FROM ITEMS WHERE DESCRIPCION="+"'"+var+"'"+"AND TIPO='HOGAR'";
            if(!var.equals("valorHogar")){
                try (Connection con =Sql2oDAO.getSql2o().open()){
                    aux=con.createQuery(consulta).executeAndFetchFirst(Item.class);
                } catch (Exception e) {
                    System.out.println(e);
                }
                seguro.addItem(aux);
            }
        }
    }
    //TODO: SI YA ACCEDIO A ESTE METODO ES PORQUE ESTA REGISTRADO O LOGUEADO, POR LO TANTO NO TENGO QUE INSERTAR EL CLIENTE
    boolean cargarSeguroHogar(String fecInicio,String fecFinal){
        SeguroHogar seguro=SeguroHogar.getSeguroHogar();
        Cliente cliente=seguro.getCliente();
        Hogar hogar=seguro.getHogar();
        int idSeguro=0;
        boolean verdad=false;
        String inserHogar="INSERT INTO HOGAR VALUES(:calle,:numero,:codigoPostal,:tipo,:valor,:localidad,:provincia)";
        String inserSeguro="INSERT INTO SEGURO VALUES(NULL,"+cliente.getDni()+",'HOGAR',1)";
        String selectId="SELECT MAX(IDSEGURO) FROM SEGURO";
        
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            con.createQuery(inserHogar).bind(hogar).executeUpdate();
            con.createQuery(inserSeguro).executeUpdate();
            idSeguro=con.createQuery(selectId).executeAndFetchFirst(Integer.class);
            System.out.println(idSeguro);
            String inserSeguroHogar="INSERT INTO SEGUROHOGAR VALUES("+idSeguro+",'"+hogar.getCalle()+"',"+hogar.getNumero()+",'"+hogar.getLocalidad()+"','"+hogar.getProvincia()+"')";
            System.out.println(inserSeguroHogar);
            con.createQuery(inserSeguroHogar).executeUpdate();
            for (Item item : seguro.getItems()){
                String insertItem="INSERT INTO ITEMSXSEGURO VALUES("+idSeguro+","+item.getCodigo()+")";
                con.createQuery(insertItem).executeUpdate();
            }
            String inserPoliza="INSERT INTO POLIZA VALUES(NULL,STR_TO_DATE("+fecInicio+",'%Y-%m-%d'),STR_TO_DATE("+fecFinal+",'%Y-%m-%d'),null,"+seguro.getCostoMensual()+","+idSeguro+","+cliente.getDni()+")";
            con.createQuery(inserPoliza).executeUpdate();
            verdad=true;
       } catch(Exception e){
            System.out.println(e);
       }

        return verdad;
    }
    //devuelve -1 si hay algun error
    int getLastId(){
        int resultado=-1;
        String sql="SELECT MAX(IDSEGURO) FROM SEGUROHOGAR";
        try (Connection con=Sql2oDAO.getSql2o().open()){
            resultado=con.createQuery(sql).executeAndFetchFirst(Integer.class);
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultado;
    }
}