package paquete;

import lombok.Data;

@Data
public class Item{
    private String descripcion;
    private int codigo;
    private float valor_mensual;
    private float cobertura;
    private String tipo;

    public Item(){
        this.descripcion="";
        this.valor_mensual=0;
        this.cobertura=0;
        this.codigo=0;
        this.tipo="";
    }
}